#!/bin/sh

# Builds the docker image and stores it locally
# Remember to first update scripts/helpers/docker_tag.sh and .gitlab-ci.yml build image name

set -e
. scripts/helpers/docker_tag.sh
podman build -f scripts/helpers/Dockerfile -t "$docker_tag" .
