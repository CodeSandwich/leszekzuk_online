# Useful commands for running the Typesense server on fly.io.
# The currently used app IDs are:
# APP_NAME=leszekzuk-online-staging-typesense
# APP_NAME=leszekzuk-online-typesense

# Log in:
# Go to https://fly.io/tokens/create and create a scoped, short-lived token
# export FLY_API_TOKEN=<the obtained token>

# Check if logged in:
# scripts/fly.sh auth whoami

# Log out, this destroys the token:
# scripts/fly.sh auth logout

# Create a new app:
# scripts/fly.sh apps create $APP_NAME

# Create a 1GB volume named `typesense_data` for the app (there can be only 1 volume per app):
# scripts/fly.sh volumes create typesense_data -a $APP_NAME --count=1 --size=1 --region=waw

# Add secrets to the app from stdin (Typesense requires `TYPESENSE_API_KEY`):
# scripts/fly.sh secrets import -a $APP_NAME

# Deploy the app from this TOML (`--ha=false` prevents the creation of redundant machines),
# this is used for both deployments of new apps and for upgrades:
# scripts/fly.sh deploy -a $APP_NAME -c scripts/typesense.fly.toml --region=waw \
#     --ha=false --vm-size=shared-cpu-1x --vm-memory=256

# SSH into the app (e.g. to `echo "$TYPESENSE_API_KEY"`):
# scripts/fly.sh ssh console -a $APP_NAME

# Display to the app logs:
# scripts/fly.sh logs -a $APP_NAME

podman run -it -e HOME=/ docker.io/flyio/flyctl:v0.3.44 -t "$FLY_API_TOKEN" $@
