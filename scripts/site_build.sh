#!/bin/sh

# Create jekyll projects based on documents in the project.
# Preconfigured for local development and being served using scripts/site_serve.sh.
# Params: <BASEURL>

set -e
export BASEURL="$1"
. scripts/helpers/docker_tag.sh
podman run \
    -e URL="http://localhost" \
    -e BASEURL \
    -e REPO_URL="http://example.com/repo" \
    -e TYPESENSE_URL="http://localhost:8108" \
    -e ALLOW_ROBOTS \
    -e TEST=1 \
    -i -v "$(pwd)/:/volume:z" -w /volume "$docker_tag" scripts/helpers/build.sh

if [ -d public ] && [ -n "$(ls public)" ]
then
    mkdir -p public_copy
    mv public/* public_copy
    mkdir -p "public/$BASEURL"
    mv public_copy/* "public/$BASEURL"
    rm -r public_copy
fi
