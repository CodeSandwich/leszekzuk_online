#!/bin/sh

# Create jekyll projects based on documents in the project.
# Env args:
# - URL: mandatory
# - BASEURL: optional, default is none
# - REPO_URL: mandatory
# - TYPESENSE_URL: mandatory
# - ALLOW_ROBOTS: optional, set to 1 to enable, default is disabled
# - TEST: optional, set to 1 to enable, default is disabled

set -e

convert_books() {
    if [ "$TEST" = "1" ]; then convert_book test.md test; fi
    convert_book 000HOMO.odt homo
    convert_book 00ARCHEO.odt archeologia
    convert_book 01chrzescij.odt swieci
    convert_book Jezusamob.docx jezus
    convert_book "Kaczy folwark.odt" folwark
    convert_book Fleck.odt fleck
    convert_book kierunkowośćEwol.odt kierunkowosc
    convert_book PersonalizmMed.odt personalizm
    convert_book SkądPrzychodzi.odt skad
    convert_book układanka.odt ukladanka
    convert_book Palimpsest.odt palimpsest
    convert_book main.md main
}

# Params: <IN_FILE> <FILTER>
convert_book () {
    echo "Converting $1"
    local FILTER="$(dirname "$0")/filters/$2.lua"
    BASE_URL="$BASEURL" REPO_URL="$REPO_URL" TYPESENSE_URL="$TYPESENSE_URL" \
        pandoc -L "$FILTER" --resource-path=inputs -t html "inputs/$1"
    echo "--------------------------------------------------"
    echo
}

build_books () {
    # Build the Jekyll projects
    for BOOK in "$TEMP_DIR"/books/*; do
        echo "Building $BOOK"
        local TEMP_OUT="$TEMP_DIR/temp_out"
        (
            cd "$BOOK"
            RUBYOPT="-W0" bundle exec jekyll build -d "$TEMP_OUT"
        )
        cp -r "$TEMP_OUT"/* "$OUT_DIR"
        rm -rf "$TEMP_OUT"
        echo
        echo "--------------------------------------------------"
        echo
    done
    # Create robots.txt
    if [ "$ALLOW_ROBOTS" != "1" ]; then
        local ROBOTS_TXT="$OUT_DIR/robots.txt"
        echo "User-agent: *" >> "$ROBOTS_TXT"
        echo "Disallow: *" >> "$ROBOTS_TXT"
    fi
    # Copy the minibar assets
    cp -r /assets "$OUT_DIR"
    # Remove the unused assets
    assets="$OUT_DIR/assets"
    rm "$assets/css/just-the-docs-default.css.map"
    rm "$assets/css/just-the-docs-light.css"
    rm "$assets/css/just-the-docs-light.css.map"
    rm "$assets/css/just-the-docs-dark.css"
    rm "$assets/css/just-the-docs-dark.css.map"
    rm "$assets/js/search-data.json"
    rm "$assets/js/vendor" -rf
    echo "Done!"
}

TEMP_DIR="$(pwd)/temp"
mkdir -p "$TEMP_DIR"
rm -rf "$TEMP_DIR"/*

OUT_DIR="$(pwd)/public"
mkdir -p "$OUT_DIR"
rm -rf "$OUT_DIR"/*

convert_books
build_books

rm -rf "$TEMP_DIR"
