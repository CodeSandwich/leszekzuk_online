local header_id = ""
local div_count = 0

local wrap_in_id_div = {}

function wrap_in_id_div.Header(header)
    header_id = header.identifier
    div_count = 0
end

function wrap_in_id_div.Para(para)
    div_count = div_count + 1
    local id = header_id.."-akapit-"..div_count
    return { pandoc.Div({}, pandoc.Attr(id, { "paragraph-anchor" })),
        pandoc.Div(para, pandoc.Attr("", { "paragraph" })) }
end

return { wrap_in_id_div }
