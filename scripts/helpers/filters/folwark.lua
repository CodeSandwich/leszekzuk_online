local utils = require("scripts.helpers.filters.utils")
local book = require("scripts.helpers.filters.book")

local fix_header = {}

function fix_header.Header(header)
    return utils.gsub_header(header, 1, "^", "Rozdział ")
end

function Pandoc(doc)
    doc = doc:walk(fix_header)
    book.write(doc, "folwark", "Kaczy folwark: baśń ze świata zwierząt",
        "Folwark Zwierzęcy to groteskowy obraz świata, gdzie idolem jest mityczna wola "
        .."większości. Widać, jak łatwo mechanizmy demokratyczne można wykoślawić aż do "
        .."autorytaryzmu w imię pieniędzy, awansu czy zemsty za własne niepowodzenia.")
    return pandoc.Pandoc{}
end
