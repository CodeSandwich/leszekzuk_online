local book = require("scripts.helpers.filters.book")

function Pandoc(doc)
    book.write(doc, "personalizm",
        "Wybrane aspekty etyki medycznej w ujęciu personalizmu chrześcijańskiego",
        "Personalizm chrześcijański w centrum uwagi nie stawia osoby "
        .."człowieka lecz postrzega człowieka wyłącznie w relacji "
        .."do Boga, co jest właściwie zaprzeczeniem personalizmu.")
    return pandoc.Pandoc{}
end
