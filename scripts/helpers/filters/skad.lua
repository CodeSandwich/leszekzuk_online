local utils = require("scripts.helpers.filters.utils")
local book = require("scripts.helpers.filters.book")

local fix_header = {}

function fix_header.Header(header)
    if(header.level == 3) then
        header.level = 4
    end
    return utils.gsub_header(header, 1, "^%d. ", "")
end

function Pandoc(doc)
    doc = doc
        :walk(utils.remove_anchors)
        :walk(utils.remove_block_quotes)
        :walk(fix_header)
    book.write(doc, "skad", "Skąd przychodzimy, dokąd zmierzamy",
        "Książka pokazuje wielowymiarowość ludzkiej istoty postrzeganej w "
        .."perspektywie kosmicznej i planetarnej. W przystępnej formie prezentuje "
        .."ewolucję człowieka, zagadnienia kultury, świadomości i życia duchowego.")
    return pandoc.Pandoc{}
end
