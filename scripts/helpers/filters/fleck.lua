local utils = require("scripts.helpers.filters.utils")
local book = require("scripts.helpers.filters.book")

local fix_header = {}

function fix_header.Header(header)
    return utils.gsub_header(header, 1, "^[IV]+$", "Rozdział %0")
end

function Pandoc(doc)
    doc = doc
        :walk(utils.remove_block_quotes)
        :walk(fix_header)
    book.write(doc, "fleck", "Trzy aspekty poznania naukowego w świetle koncepcji Ludwika Flecka",
        "Znany polsko-żydowski lekarz i filozof poznania pokazuje, że nauka tylko "
        .."w pewnym stopniu jest obiektywnym sposobem poznawania rzeczywistości. "
        .."Przede wszystkim zaś jest społeczną konwencją.")
    return pandoc.Pandoc{}
end
