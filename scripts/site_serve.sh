#!/bin/sh

# Serves the built site. It can be visited under http://localhost
# Must be run as sudo to obtain port 80.
# Only pages available under port 80 can be scraped by TypeSense docs scraper.

set -e
mkdir -p public
sudo podman run -i -v "$(pwd)/public:/public:ro,z" -p 80:8000 \
    docker.io/nabijaczleweli/http:2.1.0 --strip-extensions --mime-type ':*/*' /public
