# Czerwona linia

![Horror vacui - dualizm pustki](grafiki/Czerwona linia1.Horror vacui - dualizm pustki.jpg)

---

![Drzewa życia - multiverse](grafiki/Czerwona linia2.Drzewa życia - multiverse.jpg)

---

![Jin jang - dialektyka przeciwieństw](grafiki/Czerwona linia3.Jin jang - dialektyka przeciwieństw.jpg)

---

![Scientia czyli porządek z chaosu](grafiki/Czerwona linia4.Scientia czyli porządek z chaosu.jpg)

---

![Quo vadis?](grafiki/Czerwona linia5.Quo vadis.jpg)

---

![Deus ex machina](grafiki/Czerwona linia6.Deus ex machina.jpg)

---

![Lewiatan, feniks i nowy początek](grafiki/Czerwona linia7.Lewiatan, feniks i nowy początek.jpg)


# Dawno temu

![](grafiki/Dawno temu1.jpg)

---

![](grafiki/Dawno temu2.jpg)

---

![](grafiki/Dawno temu3.jpg)

---

![](grafiki/Dawno temu4.jpg)

---

![](grafiki/Dawno temu5.jpg)

---

![](grafiki/Dawno temu6.jpg)

# Dwoistość

![](grafiki/Dwoistość1.jpg)

---

![](grafiki/Dwoistość2.jpg)

---

![](grafiki/Dwoistość3.png)

---

![](grafiki/Dwoistość4.jpg)

---

![](grafiki/Dwoistość5.jpg)

---

![](grafiki/Dwoistość6.jpg)

---

![](grafiki/Dwoistość7.jpg)

---

![](grafiki/Dwoistość8.jpg)

# Futurologia

![](grafiki/Futurologia1.jpg)

---

![](grafiki/Futurologia2.jpg)

---

![](grafiki/Futurologia3.jpg)

# Homo

![Staję się](grafiki/Homo1.Staję się.jpg)

---

![Wobec wieczności](grafiki/Homo2.Wobec wieczności.jpg)

---

![Wola](grafiki/Homo3.Wola.jpg)

---

![Honor](grafiki/Homo4.Honor.jpg)

---

![Trybalizm](grafiki/Homo5.Trybalizm.jpg)

---

![Tradycja](grafiki/Homo6.Tradycja.jpg)

---

![Showbusiness](grafiki/Homo7.Showbusiness.jpg)

---

![Koncert](grafiki/Homo8.Koncert.jpg)

---

![Chwila refleksji](grafiki/Homo9.Chwila refleksji.jpg)

---

![Transcendencja](grafiki/Homo10.Transcendencja.jpg)

---

![Próby](grafiki/Homo11.Próby.jpg)

# Jelenia Góra

![Kotlina](grafiki/Jelenia Góra1.Kotlina.jpg)

---

![Liczyrzepa](grafiki/Jelenia Góra2.Liczyrzepa.jpg)

---

![Grodzisko](grafiki/Jelenia Góra3.Grodzisko.png)

---

![Barok](grafiki/Jelenia Góra4.Barok.jpg)

---

![Noc na cmentarzu](grafiki/Jelenia Góra5.Noc na cmentarzu.jpg)

---

![Gotyk](grafiki/Jelenia Góra6.Gotyk.jpg)

---

![Baszta](grafiki/Jelenia Góra7.Baszta.jpg)

---

![Teatr](grafiki/Jelenia Góra8.Teatr.jpg)

---

![Był tramwaj](grafiki/Jelenia Góra9.Był tramwaj.jpg)

---

![Linia murów](grafiki/Jelenia Góra10.Linia murów.jpg)

# Kobieta

![](grafiki/Kobieta1.jpg)

---

![](grafiki/Kobieta2.jpg)

---

![](grafiki/Kobieta3.jpg)

---

![](grafiki/Kobieta4.jpg)

---

![](grafiki/Kobieta5l.jpg)

---

![](grafiki/Kobieta6.jpg)

---

![](grafiki/Kobieta7.jpg)

---

![](grafiki/Kobieta8.jpg)

---

![](grafiki/Kobieta9.jpg)

---

![](grafiki/Kobieta10.jpg)

---

![](grafiki/Kobieta11.jpg)

# Medicina

![](grafiki/medicina1.jpg)

---

![](grafiki/medicina2.jpg)

---

![](grafiki/medicina3.jpg)

# Podróże

![](grafiki/Podróze1.jpg)

---

![](grafiki/Podróże2.jpg)

---

![](grafiki/Podróże3.jpg)

---

![](grafiki/Podróże4.jpg)

---

![](grafiki/Podróże5.jpg)

---

![](grafiki/Podróże6.jpg)

---

![](grafiki/Podróże7.jpg)

---

![](grafiki/Podróże8.jpg)

# Portrety

![](grafiki/Portrety1.jpg)

---

![](grafiki/Portrety2.jpg)

---

![](grafiki/Portrety3.jpg)

---

![](grafiki/Portrety4.jpg)

---

![](grafiki/Portrety5.jpg)

---

![](grafiki/Portrety7.jpg)

---

![](grafiki/Portrety8.jpg)

# Religia

![](grafiki/Religia1.png)

---

![](grafiki/Religia2.jpg)

---

![](grafiki/Religia3.jpg)

---

![](grafiki/Religia4.jpg)

---

![](grafiki/Religia5.jpg)

---

![](grafiki/Religia6.jpg)

---

![](grafiki/Religia7.jpg)

---

![](grafiki/Religia8.jpg)

# Universum

![Ogród wiedzy](grafiki/Universum1.Ogród Wiedzy.jpg)

---

![Kosmos człowieka](grafiki/Universum2.Kosmos człowieka.jpg)

---

![Tunel](grafiki/Universum3.Tunel.jpg)

---

![Woda żywa](grafiki/Universum4.Woda żywa.jpg)

# Zwierzęta

![](grafiki/Zwierzęta1.jpg)

---

![](grafiki/Zwierzęta2.jpg)

---

![](grafiki/Zwierzęta3.jpg)

---

![](grafiki/Zwierzęta4.jpg)

---

![](grafiki/Zwierzęta5.jpg)

---

![](grafiki/Zwierzęta6.jpg)

---

![](grafiki/Zwierzęta7.jpg)

---

![](grafiki/Zwierzęta8.jpg)

---

![](grafiki/Zwierzęta9.jpg)

---

![](grafiki/Zwierzęta11.jpg)

---

![](grafiki/Zwierzęta12.jpg)

---

![](grafiki/Zwierzęta13.jpg)

---

![](grafiki/Zwierzęta14.jpg)

---

![](grafiki/Zwierzęta16.jpg)

---

![](grafiki/Zwierzęta17.jpg)

---

![](grafiki/Zwierzęta18.jpg)

---

![](grafiki/Zwierzęta19.jpg)

---

![](grafiki/Zwierzęta20.jpg)
