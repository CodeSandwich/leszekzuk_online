# O mnie

Nazywam się Leszek Żuk. {{ site.description }}

Jestem doktorem habilitowanym filozofii na Uniwersytecie Wrocławskim. Na tej samej uczelni zostałem magistrem biologii oraz magistrem teologii na Papieskim Fakultecie Teologicznym. Studiowałem też mammalogię i elementy geologii.

Mieszkam we Wrocławiu. To moje miasto: tutaj wydarzyło się wszystko, co dla mnie ważne. Tu spotkałem moją żonę i tu wychowały się nasze dzieci. Tutaj mam przyjaciół. Lubię piesze wycieczki w Sudety i sporo podróżuję zarówno po Polsce, jak też świecie.

# About me

My name is Leszek Żuk. I deal with philosophy, especially philosophy of science, history, study of religions, moral science, ethics of medicine in particular, and natural sciences focusing on paleontology. Besides I write and draw. I usually illustrate my texts myself.

I have PhD in Philosophy at University of Wroclaw. On the same university I got MS in Biology and MA in Theology at Pappal Faculty of Theology. I also studied mammalogy and elements of geology.

I live in Wroclaw. This is my city: everything important in my life happened here. Here I met my wife and here my children grew up. Many of my friends live here as well. My hobby is hiking in the Sudety Mountains and traveling both in Poland and abroad.

# Publikacje

#### Książki

- *Księga liści*, Wrocław 2003, s. 249, ATUT.

- *Celowość i ukierunkowanie systemów*, Wrocław 2008, s. 122, ARGI.

- *Układanka de Deo*, Tolkmicko 2010, s. 79, Radwan.

- *Palimpsest*, Gdynia 2012, s. 342, Novae Res.

- *Rozmowy o chrześcijaństwie*, Gdynia 2012, s. 40, Novae Res.

- *Z dziejów miasta. Mons Cervi – Hirschberg – Jelenia Góra*, Jelenia Góra 2013, s, 446, Ad Rem.

- *Wspólnoty chrześcijańskie. Od herezji do ortodoksji*, Gdynia 2014, s. 536, Novae Res.

- *Skąd przychodzimy, dokąd zmierzamy*, Gdynia 2015, s. 582, Novae Res,

- *Fundamentalne koncepcje biologii w pracach Kazimierza Petrusewicza*, Jelenia Góra 2015, s. 285, Ad Rem.

- *Mgła. W powojennej Jeleniej Górze…*, Jelenia Góra 2016, s. 187, Ad Rem. (praca nagrodzona, wydana w serii Z Biblioteki Duch Gór).

#### Udział w pracach zbiorowych

- *Spadek* \[w:\] *Pisz do Pilcha. Opowiadań współczesnych trzydzieści i trzy,* Warszawa 2005, s. 257-262, Świat Książki. (tom opowiadań wyróżnionych w konkursie).

- *Trzy aspekty poznania naukowego w świetle koncepcji Ludwika Flecka* \[w:\] B. Płonka-Syroka, P. Jarnicki, B. Balicki (red.),* Ludwik Fleck. Tradycje – inspiracje – interpretacje*, Wrocław 2015, s. 49-73, Wydawnictwo Fundacji Projekt Nauka. (materiały pokonferencyjne).

- *Modele reprodukcyjne w społeczeństwach prehistorycznych* \[w:\] A. Szlagowska (red.), *Problemy zdrowia reprodukcyjnego kobiert. Tom 3. Zdrowie reprodukcyjne w kontekście zmian społeczno-kulturowych na przestrzeni wieków*, Wrocław 2020, s. 161-178, Uniwersytet Medyczny im. Piastów Śląskich we Wrocławiu.

#### Wybrane artykuły

- *Pierwsza sura Koranu*, „Znak”, 561, 2, 2002, s. 42-64.

- *Teleonomiczność w ewolucji świata*, „Ruch Filozoficzny”, 3, 2005, s. 505-516.

- *Myśląc o granicy człowieczeństwa*, „Ruch Filozoficzny”, 2, 2007, s. 209-226.

- *Teleonomia – powrót myślenia celowościowego*, „Ruch Filozoficzny”, 3, 2007, s. 411-424.

- *Stefan Röhrenschef i zmiana filozofii pszczelarstwa w XIX wieku*, „Przegląd Zoologiczny”, 1-4, 2008-2010, s. 219-225.

- *Porównanie dłoni kopalnego niedźwiedzia Ursus wenzensis Stach i współczesnego Ursus arctos L.*, „Przegląd Zoologiczny”, 1-4, 2008-2010, s. 203-210.

- *Czy odkrycie Ameryki było nieuniknione?*, „Nieznany Świat”, 9, 2010, s. 18-21, 64-66.

- *Egipt Williama Petrie*, „Archeologia Żywa”, 1, 2012, s. 70-71.


# Książki online

@@BOOK LINKS@@

# @@GRAFIKI@@
